package data

import (
	"math"
	"unsafe"
)

type SDR struct {
	size int
	data []chunk
}
type chunk = uint64

const chunkBits = int(unsafe.Sizeof(chunk(0))) * 8
const chunkNibs = chunkBits / 4
const chunkMax = ^chunk(0)

func NewSDR(size int) *SDR {
	chunkNum := (size + chunkBits - 1) / chunkBits

	return &SDR{
		data: make([]chunk, chunkNum),
		size: size,
	}
}

func (d *SDR) Size() int {
	return d.size
}

func (d *SDR) Get(n int) bool {
	byteIndex := n / chunkBits
	bit := n % chunkBits

	return d.data[byteIndex]&(1<<bit) > 0
}

func (d *SDR) Set(n int) {
	byteIndex := n / chunkBits
	bit := n % chunkBits

	d.data[byteIndex] = d.data[byteIndex] | (1 << bit)
}

func (d *SDR) Clear(n int) {
	byteIndex := n / chunkBits
	bit := n % chunkBits

	d.data[byteIndex] = d.data[byteIndex] & ^(1 << bit)
}

func (d *SDR) ClearAll() {
	for i := 0; i < len(d.data); i++ {
		d.data[i] = 0
	}
}

func (d *SDR) SetAll() {
	for i := 0; i < len(d.data); i++ {
		d.data[i] = chunkMax
	}
}

func (d *SDR) Population() int {
	match := 0

	for _, chunk := range d.data {
		for ni := 0; ni < chunkNibs; ni++ {
			match += nibbleLookup[(chunk>>(ni*4))&0x0F]
		}
	}

	return match
}

func (d *SDR) Add(rhs *SDR) {
	for i, chunk := range d.data {
		if i >= len(rhs.data) {
			break
		}

		d.data[i] = chunk | rhs.data[i]
	}
}

var nibbleLookup = []int{0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4}

func (d *SDR) Match(rhs *SDR) int {
	match := 0

	for i, chunk := range d.data {
		if i >= len(rhs.data) {
			break
		}

		overlap := chunk & rhs.data[i]

		for ni := 0; ni < chunkNibs; ni++ {
			match += nibbleLookup[(overlap>>(ni*4))&0x0F]
		}
	}

	return match
}

func (d *SDR) String() string {
	out := ""
	squareSize := int(math.Ceil(math.Sqrt(float64(d.size))))
	for row := 0; row < squareSize; row++ {
		for col := 0; col < squareSize; col++ {
			i := row*squareSize + col
			if i >= d.size {
				break
			}

			if d.Get(i) {
				out += "1 "
			} else {
				out += "0 "
			}
		}
		out += "\n"
	}

	return out
}
