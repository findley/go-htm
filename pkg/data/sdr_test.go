package data

import (
	"testing"
)

func TestGet_EmptyExpectFalse(t *testing.T) {
	// Arrange
	d := NewSDR(100)

	// Act
	got := d.Get(50)

	// Assert
	if got != false {
		t.Errorf("Expected false got %t", got)
	}
}

func TestGet_SetBitExpectTrue(t *testing.T) {
	// Arrange
	d := NewSDR(20)

	// Act
	d.Set(10)
	got := d.Get(10)

	// Assert
	if got != true {
		t.Errorf("Expected true got %t", got)
	}
}

func TestGet_ClearedBitExpectFalse(t *testing.T) {
	// Arrange
	d := NewSDR(100)

	// Act
	d.Set(50)
	d.Clear(50)
	got := d.Get(50)

	// Assert
	if got != false {
		t.Errorf("Expected false got %t", got)
	}
}

func TestAdd_BothFalseResultFalse(t *testing.T) {
	// Arrange
	d1 := NewSDR(20)
	d2 := NewSDR(20)

	// Act
	d1.Add(d2)
	got := d1.Get(5)

	// Assert
	if got != false {
		t.Errorf("Expected false got %t", got)
	}
}

func TestAdd_OneTrueResultTrue(t *testing.T) {
	// Arrange
	d1 := NewSDR(20)
	d2 := NewSDR(20)
	d1.Set(5)

	// Act
	d1.Add(d2)
	got := d1.Get(5)

	// Assert
	if got != true {
		t.Errorf("Expected true got %t", got)
	}
}

func TestAdd_BothTrueResultTrue(t *testing.T) {
	// Arrange
	d1 := NewSDR(20)
	d2 := NewSDR(20)
	d1.Set(5)
	d2.Set(5)

	// Act
	d1.Add(d2)
	got := d1.Get(5)

	// Assert
	if got != true {
		t.Errorf("Expected true got %t", got)
	}
}

func TestAdd_IndependentTrueBothTrue(t *testing.T) {
	// Arrange
	d1 := NewSDR(20)
	d2 := NewSDR(20)
	d1.Set(5)
	d2.Set(7)

	// Act
	d1.Add(d2)
	v1 := d1.Get(5)
	v2 := d2.Get(7)

	// Assert
	if v1 && v2 != true {
		t.Errorf("Expected true got %t", v1 && v2)
	}
}

func TestMatch_BothEmptyExpect0Match(t *testing.T) {
	// Arrange
	d1 := NewSDR(20)
	d2 := NewSDR(20)

	// Act
	got := d1.Match(d2)

	// Assert
	if got != 0 {
		t.Errorf("Expected 0 got %d", got)
	}
}

func TestMatch_NoOverlapExpect0Match(t *testing.T) {
	// Arrange
	d1 := NewSDR(20)
	d2 := NewSDR(20)

	d1.Set(0)
	d1.Set(2)
	d1.Set(4)
	d1.Set(6)
	d1.Set(8)
	d1.Set(10)

	d2.Set(1)
	d2.Set(3)
	d2.Set(5)
	d2.Set(7)
	d2.Set(9)
	d2.Set(11)

	// Act
	got := d1.Match(d2)

	// Assert
	if got != 0 {
		t.Errorf("Expected 0 got %d", got)
	}
}

func TestMatch_1OverlapExpect1Match(t *testing.T) {
	// Arrange
	d1 := NewSDR(20)
	d2 := NewSDR(20)

	d1.Set(5)
	d2.Set(5)

	// Act
	got := d1.Match(d2)

	// Assert
	if got != 1 {
		t.Errorf("Expected 1 got %d", got)
	}
}

func TestMatch_2OverlapExpect2Match(t *testing.T) {
	// Arrange
	d1 := NewSDR(20)
	d2 := NewSDR(20)

	d1.Set(5)
	d2.Set(5)
	d1.Set(0)
	d2.Set(0)

	// Act
	got := d1.Match(d2)

	// Assert
	if got != 2 {
		t.Errorf("Expected 2 got %d", got)
	}
}

func TestMatch_FullOverlapExpectFullMatch(t *testing.T) {
	// Arrange
	size := 16
	d1 := NewSDR(size)
	d2 := NewSDR(size)

	for i := 0; i < size; i++ {
		d1.Set(i)
		d2.Set(i)
	}

	// Act
	got := d1.Match(d2)

	// Assert
	if got != size {
		t.Errorf("Expected %d got %d", size, got)
	}
}

func TestClearAll_ExpectAllSetBitsCleared(t *testing.T) {
	// Arrange
	size := 2048
	indicies := []int{1481, 545, 1412, 605, 562, 1009, 189, 1052, 172, 640, 129, 329, 1686, 1515, 1030, 1934, 1377, 168, 940, 387, 167, 1545, 1896, 732, 723, 1735, 1594, 12, 858, 998, 528, 1015, 2037, 151, 1665, 51, 962, 497, 986, 1941}

	d := NewSDR(size)
	for _, ind := range indicies {
		d.Set(ind)
	}

	// Act
	d.ClearAll()

	// Assert
	for i, chunk := range d.data {
		if chunk != 0 {
			t.Errorf("Expected 0 got %d at index %d", chunk, i)
		}
	}
}

func TestPopulation_0SetExpect0(t *testing.T) {
	// Arrange
	d := NewSDR(100)

	// Act
	got := d.Population()

	// Assert
	if got != 0 {
		t.Errorf("Expected 0 got %d", got)
	}
}

func TestPopulation_1SetExpect1(t *testing.T) {
	// Arrange
	d := NewSDR(100)
	d.Set(44)

	// Act
	got := d.Population()

	// Assert
	if got != 1 {
		t.Errorf("Expected 1 got %d", got)
	}
}

func TestPopulation_40SetExpect40(t *testing.T) {
	// Arrange
	size := 2048
	indicies := []int{1481, 545, 1412, 605, 562, 1009, 189, 1052, 172, 640, 129, 329, 1686, 1515, 1030, 1934, 1377, 168, 940, 387, 167, 1545, 1896, 732, 723, 1735, 1594, 12, 858, 998, 528, 1015, 2037, 151, 1665, 51, 962, 497, 986, 1941}

	d := NewSDR(size)
	for _, ind := range indicies {
		d.Set(ind)
	}

	// Act
	got := d.Population()

	// Assert
	if got != 40 {
		t.Errorf("Expected 40 got %d", got)
	}
}

func BenchmarkCreate(b *testing.B) {
	size := 2048
	indicies := []int{1481, 545, 1412, 605, 562, 1009, 189, 1052, 172, 640, 129, 329, 1686, 1515, 1030, 1934, 1377, 168, 940, 387, 167, 1545, 1896, 732, 723, 1735, 1594, 12, 858, 998, 528, 1015, 2037, 151, 1665, 51, 962, 497, 986, 1941}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		d := NewSDR(size)
		for _, ind := range indicies {
			d.Set(ind)
		}
	}
}

func BenchmarkRandomRead(b *testing.B) {
	size := 2048
	indicies := []int{1481, 545, 1412, 605, 562, 1009, 189, 1052, 172, 640, 129, 329, 1686, 1515, 1030, 1934, 1377, 168, 940, 387, 167, 1545, 1896, 732, 723, 1735, 1594, 12, 858, 998, 528, 1015, 2037, 151, 1665, 51, 962, 497, 986, 1941}

	d := NewSDR(size)
	for _, ind := range indicies {
		d.Set(ind)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		d.Get(i % len(indicies))
	}
}

func BenchmarkMatch(b *testing.B) {
	size := 2048
	indicies1 := []int{1481, 545, 1412, 605, 562, 1009, 189, 1052, 172, 640, 129, 329, 1686, 1515, 1030, 1934, 1377, 168, 940, 387, 167, 1545, 1896, 732, 723, 1735, 1594, 12, 858, 998, 528, 1015, 2037, 151, 1665, 51, 962, 497, 986, 1941}
	indicies2 := []int{1481, 545, 1412, 605, 562, 1009, 189, 1052, 172, 640, 129, 329, 1686, 1515, 1030, 1934, 1377, 168, 940, 387, 167, 1545, 1896, 732, 723, 1735, 1594, 12, 858, 998, 528, 1015, 2037, 151, 1665, 51, 962, 497, 986, 1941}

	d1 := NewSDR(size)
	d2 := NewSDR(size)

	for _, ind := range indicies1 {
		d1.Set(ind)
	}

	for _, ind := range indicies2 {
		d2.Set(ind)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		d1.Match(d2)
	}
}

func BenchmarkAdd(b *testing.B) {
	size := 2048
	indicies1 := []int{1481, 545, 1412, 605, 562, 1009, 189, 1052, 172, 640, 129, 329, 1686, 1515, 1030, 1934, 1377, 168, 940, 387, 167, 1545, 1896, 732, 723, 1735, 1594, 12, 858, 998, 528, 1015, 2037, 151, 1665, 51, 962, 497, 986, 1941}
	indicies2 := []int{153, 1418, 1678, 398, 1892, 617, 389, 1165, 1091, 1470, 1422, 245, 1888, 1477, 1384, 1638, 177, 459, 491, 372, 926, 2019, 447, 1082, 123, 317, 814, 851, 622, 27, 85, 1500, 1014, 580, 1361, 1284, 576, 132, 799, 1616}

	d1 := NewSDR(size)
	d2 := NewSDR(size)

	for _, ind := range indicies1 {
		d1.Set(ind)
	}

	for _, ind := range indicies2 {
		d2.Set(ind)
	}

	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		d1.Add(d2)
	}
}
