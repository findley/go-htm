package encode

import (
	"htm/pkg/data"
	"math"
)

type ScalarEncoder struct {
	size        int
	outputWidth int
	numBuckets  int

	minValue float64
	maxValue float64

	recycle []*data.SDR
}

func NewScalarEncoder(size, outputWidth int, min, max float64) *ScalarEncoder {
	e := &ScalarEncoder{
		size:        size,
		outputWidth: outputWidth,
		numBuckets:  size - outputWidth + 1,
		minValue:    min,
		maxValue:    max,
		recycle:     make([]*data.SDR, 0),
	}

	return e
}

func (e *ScalarEncoder) Encode(value float64) *data.SDR {
	d := e.nextSDR()
	bucket := int(math.Round(value / (e.maxValue - e.minValue) * float64(e.numBuckets)))

	for i := bucket; i < bucket+e.outputWidth; i++ {
		if i >= d.Size() {
			break
		}
		d.Set(i)
	}

	return d
}

func (e *ScalarEncoder) RecycleSDR(d *data.SDR) {
	e.recycle = append(e.recycle, d)
}

func (e *ScalarEncoder) nextSDR() *data.SDR {
	if len(e.recycle) == 0 {
		return data.NewSDR(e.size)
	}

	var d *data.SDR
	d, e.recycle = e.recycle[0], e.recycle[1:]
	d.ClearAll()

	return d
}
