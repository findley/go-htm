package encode

import (
	"testing"
)

func TestEncode_Scalar_Basic(t *testing.T) {
	// Arrange
	e := NewScalarEncoder(10, 2, 0, 100)
	expected := []int{3, 4}

	// Act
	d := e.Encode(35)
	pop := d.Population()

	// Assert
	if pop != 2 {
		t.Errorf("Expected population to be 2, got %d", pop)
	}
	for _, ind := range expected {
		if d.Get(ind) != true {
			t.Errorf("Expected bit %d to be true, got false", ind)
		}
	}
}

func TestEncode_Scalar_ExpectOverlap(t *testing.T) {
	// Arrange
	e := NewScalarEncoder(100, 10, 0, 90)

	// Act
	d1 := e.Encode(55)
	d2 := e.Encode(56)
	pop1 := d1.Population()
	pop2 := d2.Population()
	overlap := d1.Match(d2)

	// Assert
	if pop1 != 10 {
		t.Errorf("Expected population to be 10, got %d", pop1)
	}
	if pop2 != 10 {
		t.Errorf("Expected population to be 10, got %d", pop2)
	}
	if overlap != 9 {
		t.Errorf("Expected overlap to be 9, got %d", overlap)
	}
}

func TestEncode_Scalar_ExpectNoOverlap(t *testing.T) {
	// Arrange
	e := NewScalarEncoder(100, 10, 0, 90)

	// Act
	d1 := e.Encode(55)
	d2 := e.Encode(65)
	pop1 := d1.Population()
	pop2 := d2.Population()
	overlap := d1.Match(d2)

	// Assert
	if pop1 != 10 {
		t.Errorf("Expected population to be 10, got %d", pop1)
	}
	if pop2 != 10 {
		t.Errorf("Expected population to be 10, got %d", pop2)
	}
	if overlap != 0 {
		t.Errorf("Expected overlap to be 0, got %d", overlap)
	}
}
