package spatial

import (
	"math"
	"math/rand"

	"htm/pkg/data"
)

type Col struct {
	Conns        *data.SDR
	Permanances  []float64
	CurrentMatch int
}

func NewCol(inputSize int) *Col {
	return &Col{
		Conns:       data.NewSDR(inputSize),
		Permanances: make([]float64, inputSize),
	}
}

func (c *Col) Init(potentialPercent, permThreshold float64) {
	potentialPool := int(math.Round(potentialPercent * float64(c.Conns.Size())))
	pathways := rand.Perm(c.Conns.Size())[:potentialPool]

	for _, j := range pathways {
		perm := clampPerm(rand.NormFloat64()*0.1 + permThreshold)
		c.Permanances[j] = perm
		if perm >= permThreshold {
			c.Conns.Set(j)
		}
	}
}

func (c *Col) learn(input *data.SDR, options *Options) {
	for j, perm := range c.Permanances {
		if perm == 0 {
			// perm values of 0 indicate never connect
			continue
		}

		if input.Get(j) {
			p := clampPerm(c.Permanances[j] + options.SynapticPermanenceInc)
			c.Permanances[j] = p
			if p >= options.PermanenceThreshold {
				c.Conns.Set(j)
			}
		} else {
			p := clampPerm(c.Permanances[j] - options.SynapticPermanenceDec)
			c.Permanances[j] = p
			if p < options.PermanenceThreshold {
				c.Conns.Clear(j)
			}
		}
	}
}

func clampPerm(val float64) float64 {
	if val < 0.000001 {
		return 0.000001
	}

	if val > 1 {
		return 1
	}

	return val
}
