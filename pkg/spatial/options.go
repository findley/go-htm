package spatial

type Options struct {
	PotentialPercent      float64
	ActivationDensity     float64
	StimulusThreshold     int
	PermanenceThreshold   float64
	SynapticPermanenceInc float64
	SynapticPermanenceDec float64
	PotentialInhibtRadius float64
	GlobalInhibit         bool
	DutyCyclePeriod       int
	MaxBoost              int
	Periodic              bool
}

func DefaultOptions() *Options {
	return &Options{
		PotentialPercent:      0.8,
		ActivationDensity:     0.02,
		StimulusThreshold:     1,
		PermanenceThreshold:   0.5,
		SynapticPermanenceInc: 0.05,
		SynapticPermanenceDec: 0.008,
		PotentialInhibtRadius: 5,
		GlobalInhibit:         true,
		DutyCyclePeriod:       1000,
		MaxBoost:              1,
		Periodic:              false,
	}
}
