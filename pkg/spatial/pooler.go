package spatial

import (
	"container/list"
	"htm/pkg/data"
)

type Pooler struct {
	inputSize        int
	outputWidth      int
	outputHeight     int
	outputSize       int
	learningEnabled  bool
	activationTarget int
	options          *Options
	cols             []*Col
}

func NewPooler(inputSize, outWidth, outHeight int, options *Options) *Pooler {
	outputSize := outWidth * outHeight

	return &Pooler{
		options:          options,
		inputSize:        inputSize,
		outputWidth:      outWidth,
		outputHeight:     outHeight,
		outputSize:       outputSize,
		learningEnabled:  true,
		activationTarget: int(options.ActivationDensity * float64(outputSize)),
		cols:             make([]*Col, outputSize),
	}
}

func (p *Pooler) init() {
	for i := 0; i < p.outputSize; i++ {
		col := NewCol(p.inputSize)
		col.Init(p.options.PotentialPercent, p.options.PermanenceThreshold)
		p.cols[i] = col
	}
}

func (p *Pooler) Run(input *data.SDR) *data.SDR {
    if input.Size() != p.inputSize {
        panic("The SDR provided to the SP did not match the expected input size")
    }

	output := data.NewSDR(p.outputSize)

	for _, col := range p.cols {
		col.CurrentMatch = col.Conns.Match(input)
	}

	activationRank := list.New()

	for i := range p.cols {
		insertIndexSorted(i, activationRank, p.cols)
	}

	el := activationRank.Front()
	for i := 0; i < p.activationTarget; i++ {
		if idx, ok := el.Value.(int); ok {
			if p.cols[idx].CurrentMatch >= p.options.StimulusThreshold {
				output.Set(idx)
			}
		}
		el = el.Next()
	}

	if p.learningEnabled {
		p.learn(input, output)
	}

	return output
}

func (p *Pooler) learn(input, output *data.SDR) {
	for i, col := range p.cols {
		if output.Get(i) {
			col.learn(input, p.options)
		}
	}
}

func (p *Pooler) EnableLearning() {
	p.learningEnabled = true
}

func (p *Pooler) DisableLearning() {
	p.learningEnabled = false
}

func insertIndexSorted(i int, activationRank *list.List, cols []*Col) {
	el := activationRank.Front()

	for {
		if el == nil {
			activationRank.PushBack(i)
			return
		}

		if cols[i].CurrentMatch > cols[el.Value.(int)].CurrentMatch {
			activationRank.InsertBefore(i, el)
			return
		}

		el = el.Next()
	}
}
