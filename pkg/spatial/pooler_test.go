package spatial

import (
	"fmt"
	"htm/pkg/data"
	"math"
	"testing"
)

const percentEpsilon = 0.005

func TestInit(t *testing.T) {
	// Arrange
	options := DefaultOptions()
	p := NewPooler(100, 100, 1, options)

	// Act
	p.init()

	// Assert
	if len(p.cols) != 100 {
		t.Errorf("Expected 100 columns, got %d", len(p.cols))
	}

	for _, col := range p.cols {
		potentialPool := 0
		for _, perm := range col.Permanances {
			if perm > 0 {
				potentialPool += 1
			}
		}
		density := float64(potentialPool) / float64(col.Conns.Size())
		if math.Abs(density-options.PotentialPercent) > percentEpsilon {
			t.Errorf("Expected potential percent to be %f, got %f", options.PotentialPercent, density)
		}
	}
}

func TestRun_ActivationDensity(t *testing.T) {
	// Arrange
	options := DefaultOptions()
	p := NewPooler(100, 100, 1, options)
	p.init()
	input := data.NewSDR(100)
	for i := 0; i < 30; i++ {
		input.Set(i)
	}

	// Act
	output := p.Run(input)
	fmt.Println("input:")
	fmt.Println(input)

	fmt.Println("cell[22] connections to input space:")
	fmt.Println(p.cols[22].Conns)

	fmt.Println("output:")
	fmt.Println(output)

	// Assert
	density := float64(output.Population()) / float64(p.outputSize)
	if math.Abs(density-options.ActivationDensity) > percentEpsilon {
		t.Errorf("Expected activation density to be %f, got %f", options.ActivationDensity, density)
	}
}

func TestRun_OverlapSemantics(t *testing.T) {
	// Arrange
	options := DefaultOptions()
	options.ActivationDensity = 0.05
	p := NewPooler(1000, 1000, 1, options)
	p.init()
	p.DisableLearning()

	d1 := data.NewSDR(1000) // Fixed SDR
	d2 := data.NewSDR(1000) // Sliding SDR

	for i := 0; i < 50; i++ {
		d1.Set(i)
	}

	// Act
	outputOverlaps := make([]float64, 0)
	for offset := 1; offset <= 50; offset += 5 {
		for i := 0; i < 50; i++ {
			d1.Set(i)
			d2.Set(i + offset)
		}

		out1 := p.Run(d1)
		out2 := p.Run(d2)
		outputOverlap := float64(out1.Match(out2)) / float64(out1.Population())
		outputOverlaps = append(outputOverlaps, outputOverlap)
		d2.ClearAll()
	}

	// Assert
	lastOverlap := 1.0
	score := 0
	for _, overlap := range outputOverlaps {
		if overlap < lastOverlap {
			score += 1
		}
		lastOverlap = overlap
	}

	if score < len(outputOverlaps)-1 {
		t.Errorf("Expected no more than 1 semantic overlap errors got %d", len(outputOverlaps)-score)
	}
}

func TestRun_HebbianLearningTrend(t *testing.T) {
	// Arrange
	size := 100
	options := DefaultOptions()
	p := NewPooler(size, size, 1, options)
	p.init()

	d := data.NewSDR(size)
	for i := 0; i < 20; i++ {
		d.Set(i)
	}

	di := data.NewSDR(size)
	for i := 0; i < 100; i++ {
		if !d.Get(i) {
			di.Set(i)
		}
	}

	// Act
	positiveOverlaps := make([]int, 0)
	negativeOverlaps := make([]int, 0)
	for i := 0; i < 5; i++ {
		output := p.Run(d)
		for j := 0; j < size; j++ {
			if output.Get(j) {
				positiveOverlaps = append(positiveOverlaps, p.cols[j].Conns.Match(d))
				negativeOverlaps = append(negativeOverlaps, p.cols[j].Conns.Match(di))
				break
			}
		}
	}

	// Assert
	lastOverlap := 0
	score := 0
	for _, overlap := range positiveOverlaps {
		if overlap >= lastOverlap {
			score += 1
		}
		lastOverlap = overlap
	}

	if score < len(positiveOverlaps)-1 {
		t.Errorf("Expected no more than 1 learning errors got %d", len(positiveOverlaps)-score)
	}

	if positiveOverlaps[len(positiveOverlaps)-1] <= positiveOverlaps[0] {
		t.Errorf("Expected connections to active input region to increase")
	}

	if negativeOverlaps[len(negativeOverlaps)-1] >= negativeOverlaps[0] {
		t.Errorf("Expected connections to inactive input region to decrease")
	}
}
